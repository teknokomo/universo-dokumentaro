.. Grave! Important! Важно!

.. Proletoj el ĉiuj landoj, unuiĝu!
   Workers of the world, unite!
   Пролетарии всех стран, соединяйтесь!

.. https://universo.pro/!

Для разработчиков
=================

.. note::

   Этот материал находится в состоянии наполнения информацией.

.. toctree::
   :maxdepth: 1
   :name: sec-konstrukciisto

   enkonduko
   lernado/contrib
   aktiveco-objektoj
   distribucio-sxargxo
   esencoj-por-objektoj
   esencoj-por-organizo
   esencoj-por-uzanto
   kategorioj-resursoj
   por-novaj-komplicoj
   rajto-posedo
   sxablonoj
   formado-kodo
   
   
   