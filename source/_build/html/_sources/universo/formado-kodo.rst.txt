..    Пролетарии всех стран, соединяйтесь!
      Proletoj el ĉiuj landoj, unuiĝu!
.. vim: syntax=rst
.. _contrib:
.. highlight:: bash

Стандарты оформления кода
=========================

Общая информация
----------------

Стандарты оформления кода в рамках разработки Реального виртуального мира Универсо складываются из рекомендаций / стандартов тех технологий которые используются в рамках создания Универсо и дополнительных рекомендаций уже исходя нашей собственной практики работы.

Рекомендации по Godot / GDScript
---------------------------------

Самые общие рекомендации по GDScript даны в официальной документации в разделе `"Руководство по стилю GDScript" <https://docs.godotengine.org/ru/latest/getting_started/scripting/gdscript/gdscript_styleguide.html>`_. Ссылка дана на версию документа на русском языке, но некоторые блоки там переведены.

Там, в частности сказано, что "Поскольку GDScript очень близок к Python, это руководство вдохновлено руководством по стилю программирования на Python PEP 8". Если что можно и про Python PEP 8 почитать, например `вот тут <https://geekbrains.ru/posts/pep8>`_ материал со ссылками на версии на разных языках. НО! Нужно учитывать, что стандарты могут отличаться. В частности в GDScript рекомендованным считается боковой отступ табом, а в Python четырьмя пробелами.

Основное
~~~~~~~~

1. Выше уже сказано, но важно повторить, что в GDScript для боковых отступов нужно использовать Tab, а не пробелы.

2. Файлы называть с маленькой буквой, например, ``queries.gd``. При необходимости можно использовать знаки подчёркивания, так называемую "змеиную_нотацию" или по другому "змеиный_регистр", например, ``resursa_centro.gd``.

Рекомендации по Universo
-------------------------

Основное
~~~~~~~~

1. Стараемся, по возможности давать названия всему на международном языке (эсперанто), если что вот есть словарь `<http://rueo.ru/>`_ но если не получается, то стараемся на английском языке. Лучше избегать названий транслитом на русском, сербском, болгарском и т.д. языках.

2. Пишем много комментариев кода... и пишем комментарии, получается, сейчас на русском языке, раз это сейчас наш основной рабочий язык. Комментарии пишем хоть к каждой строчке, некоторый код у нас так и прокомментирован. Будут опытные занудные программисты, которые будут говорить что это не удобно, что это снижает читаемость кода, куча комментариев отвлекают. Этот всё вопрос привычки, а много комментариев кода соответствуют нашим целям. 

Общая структура проекта
~~~~~~~~~~~~~~~~~~~~~~~

Код делим на различные кусочки - блоки связанные общей близкой логикой. В клиентском и сервером приложении Универсо могут быть следующие корневые папки (не обязательно сейчас все из них есть, некоторые просто запланированы):

- ``kerno`` - ядро, тут важные, всеобщие для проекта модули / файлы.
- ``blokoj`` - основные функциональные блоки приложения, основной функционал как раз находится здесь разделённый на папки-блоки.
- ``resursoj`` - директория верхнего уровня для папок video, audio и других для хранения различных общих для проекта "ресурсов".
- ``skriptoj`` - для общих скриптов проекта, которые при этом не являются частью ядра. 
- ``testoj`` - для общих тестов.

Также в корне проекта могут находится некоторые общие файлы, например:

- ``project.godot`` - главный файл проекта Godot.
- ``README.md`` - файл общего описания проекта.

При необходимости могут быть созданы другие общие папки для каких-то сущностей, которые нужно сгруппировать отдельно (например шрифты, шейдеры и т.д.) и другие отдельные общие файлы.

Структура блока
~~~~~~~~~~~~~~~~

Блоки функционала (которые папки внутри папки blokoj) называем маленькими латинскими буквами, например, ``rajtigo`` - блок с функционалом авторизации. При необходимости в качестве разделителя можно использовать тире, например, ``resursa-centro`` - блок с функционалом ресурсного центра.

Внутри блоков функционала структура частично схожая с папками верхнего уровня, только в блоках содержатся сущности касающиеся конкретного блока.

- ``lingvoj`` - файлы локализации на разные языки.
- ``scenoj`` - сцены относящиеся непосредственно к конкретному блоку.
- ``skriptoj`` - скрипты относящиеся непосредственно к конкретному блоку.
- ``testoj`` - тесты относящиеся непосредственно к конкретному блоку.

Также в корне проекта могут находится некоторые общие файлы, как правило это:

- ``xxx.tscn`` файл корневой сцены блока, вместо указанного ``xxx`` этот файл называется так же как и блок.
- ``xxx.gd`` файл корневого скрипта блока, вместо указанного ``xxx`` этот файл называется так же как и блок.

При необходимости могут быть созданы другие папки блока для каких-то сущностей, которые нужно сгруппировать отдельно (например шрифты, шейдеры и т.д.) и другие отдельные файлы блока.



















