.. Grave! Important! Важно!

.. Proletoj el ĉiuj landoj, unuiĝu!
   Workers of the world, unite!
   Пролетарии всех стран, соединяйтесь!

.. https://universo.pro/

Оглавление
==========

.. note::

   Этот материал находится в состоянии наполнения информацией.

.. toctree::
   :maxdepth: 1
   :name: sec-django-servilo

   strukturo
   panelo/index
   panelo/objektoj
   panelo/resursoj


