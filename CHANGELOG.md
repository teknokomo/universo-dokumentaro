# 0.8.0-alfo (2020-07-26)

## Новый функционал / Features:

- [Перенести из Вики в проект документации полное описание Реального виртуального мира Универсо](https://gitlab.com/tehnokom/universo-dokumentaro/-/issues/4)
- [Проанализировать необходимость разделение документации на различные репозитории](https://gitlab.com/tehnokom/universo-dokumentaro/-/issues/5)
- [Провести реорганизацию документации после выделения документации Универсо в отдельный проект](https://gitlab.com/tehnokom/universo-dokumentaro/-/issues/6)
- [Изучить возможность указать разные версии для разных приложений в навигации](https://gitlab.com/tehnokom/universo-dokumentaro/-/issues/7)
- [Изучить возможность перенести проект на наш домен](https://gitlab.com/tehnokom/universo-dokumentaro/-/issues/8)
- [Перенос описания структуры проекта](https://gitlab.com/tehnokom/universo-dokumentaro/-/issues/9)
- [Изменить название файлов документации на Илингво-написание](https://gitlab.com/tehnokom/universo-dokumentaro/-/issues/10)
- [Перенести документацию из Вики Клиентского приложения и Вики Серверного приложения](https://gitlab.com/tehnokom/universo-dokumentaro/-/issues/11)
- [Обновить документацию по списку Джанго-приложений](https://gitlab.com/tehnokom/universo-dokumentaro/-/issues/12)
- [Сделать документацию на создание ресурсов и их связей](https://gitlab.com/tehnokom/universo-dokumentaro/-/issues/14)
- [Разместить в двух базовых виртуальных мирах астероиды](https://gitlab.com/tehnokom/universo-dokumentaro/-/issues/15)
- [Создать документацию на создание объектов и их связей](https://gitlab.com/tehnokom/universo-dokumentaro/-/issues/19)